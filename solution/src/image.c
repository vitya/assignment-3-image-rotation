#include "include/image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pixel pixel_from_rgb(uint8_t red, uint8_t green, uint8_t blue) {
  return (struct pixel){blue, green, red};
}

struct image *image_new(size_t width, size_t height) {
  struct image *image = malloc(sizeof(struct image));
  image->width = width;
  image->height = height;
  image->pixels = malloc(sizeof(struct pixel) * width * height);
  return image;
}

struct pixel *image_row(struct image const *img, size_t row) {
  return img->pixels + row * img->width;
}

struct pixel *image_pixel(struct image const *img, size_t row, size_t column) {
  return image_row(img, row) + column;
}

void image_free(struct image *img) {
  free(img->pixels);
  free(img);
}
