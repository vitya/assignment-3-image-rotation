#include "include/imagetransform.h"
#include "include/image.h"
#include <stdio.h>

struct image *image_rotate_90_degrees_counterclockwise(struct image const *origin) {
  struct image *rotated = image_new(origin->height, origin->width);
  for (size_t i = 0; i < origin->height; ++i) {
    for (size_t j = 0; j < origin->width; ++j) {
      *image_pixel(rotated, origin->width - 1 - j, i) =
          *image_pixel(origin, i, j);
    }
  }
  return rotated;
}
