#ifndef _BMP_BMP_H_
#define _BMP_BMP_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_file_headers {
  char file_type[2];
  uint32_t file_size;
  uint16_t reserved_0;
  uint16_t reserved_1;
  uint32_t data_offset;
};

struct __attribute__((packed)) bmp_info_headers {
  uint32_t size;
  uint32_t width;
  uint32_t height;
  uint16_t color_planes_count;
  uint16_t bits_per_pixel;
  uint32_t compression;
  uint32_t image_size;
  uint32_t x_pixels_count_per_meter;
  uint32_t y_pixels_count_per_meter;
  uint32_t used_colors_count;
  uint32_t important_colors_count;
};

struct __attribute__((packed)) bmp_color_headers {
  uint32_t red_mask;
  uint32_t green_mask;
  uint32_t blue_mask;
  uint32_t alpha_mask;
  uint32_t color_space_type;
  uint32_t unused[16];
};

struct __attribute__((packed)) bmp_headers {
  struct bmp_file_headers file;
  struct bmp_info_headers info;
  struct bmp_color_headers color;
};

typedef struct {
  uint8_t *raw;
} bmp_image_ptr;

struct bmp_headers *bmp_image_headers(bmp_image_ptr bmp);
uint8_t *bmp_image_data(bmp_image_ptr bmp);
void bmp_image_free(bmp_image_ptr bmp);

#endif
