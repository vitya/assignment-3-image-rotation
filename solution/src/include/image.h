#ifndef _IMAGE_IMAGE_H_
#define _IMAGE_IMAGE_H_

#include <inttypes.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
};

struct image {
  size_t width;
  size_t height;

  // packed array with size
  // sizeof(pixel) * width * height
  struct pixel *pixels; 
};

struct image *image_new(size_t width, size_t height);
struct pixel *image_row(struct image const *img, size_t row);
struct pixel *image_pixel(struct image const *img, size_t row, size_t column);
void image_free(struct image *img);

#endif
