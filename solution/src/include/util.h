#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdlib.h>

void memcopy(void *dest, void *src, size_t n);
void memcopy_reversed(void *dest, void *src, size_t n);

#endif
